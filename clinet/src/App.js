import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import DaumPost from "./components/daumpost/daumpost";
import Header from "./components/header/header";
import Join from "./components/join/join";
import Login from "./components/login/login";
import Main from "./components/main/main";
import Test from "./components/test/test";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/test" element={<Test />} />
          <Route path="/login" element={<Login />} />
          <Route path="/join" element={<Join />} />
          <Route path="/post" element={<DaumPost />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
