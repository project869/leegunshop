import { Link } from "@mui/material";
import {
  Avatar,
  Button,
  Card,
  Col,
  Dropdown,
  Grid,
  Input,
  Menu,
  Row,
  Space,
  Tag,
} from "antd";

import SubMenu from "antd/lib/menu/SubMenu";
import React from "react";
import logo from "../../logo192.png";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import { EuroOutlined, MenuOutlined } from "@mui/icons-material";
import styles from "./test.module.css";
import {
  BarsOutlined,
  CalculatorOutlined,
  HeartOutlined,
  SearchOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from "@ant-design/icons";

import axiosClient from "../../apis/index.js";

const { useBreakpoint } = Grid;

const Test = () => {
  const dummy = {
    nickname: "Dogveloper",
    Post: [],
    Followings: [],
    Followers: [],
  };
  const children = "test";
  const screens = useBreakpoint();
  console.log(process.env.REACT_APP_API_URL);
  const getTest = () => {
    axiosClient.get("/users").then((res) => {
      console.log(res);
    });
  };

  getTest();

  const menu = (
    <Menu
      items={[
        {
          key: "1",
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.antgroup.com"
            >
              1st menu item
            </a>
          ),
        },
        {
          key: "2",
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.aliyun.com"
            >
              2nd menu item
            </a>
          ),
        },
        {
          key: "3",
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.luohanacademy.com"
            >
              3rd menu item
            </a>
          ),
        },
      ]}
    />
  );

  const menuCommunity = (
    <Menu
      style={{
        backgroundColor: "black",
      }}
      items={[
        {
          key: "1",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.antgroup.com"
            >
              공지사항
            </a>
          ),
        },
        {
          key: "2",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.aliyun.com"
            >
              상품 Q&A
            </a>
          ),
        },
        {
          key: "3",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.luohanacademy.com"
            >
              자유게시판
            </a>
          ),
        },
      ]}
    />
  );

  return (
    <>
      <div>
        <ShoppingCartOutlinedIcon />
        <Menu
          mode="horizontal"
          className={styles.menu}
          key="menu1"
          overflowedIndicator={<MenuOutlined />}
          selectable="false"
          triggerSubMenuAction="click"
        >
          <Menu.Item>
            <a href="/#"> 노드버드 </a>
          </Menu.Item>
          <Menu.Item
            style={{
              marginRight: "40px",
            }}
          >
            <a href="/#"> 프로필 </a>
          </Menu.Item>
          <Menu.Item>
            <a href="/#"> 회원가입 </a>
          </Menu.Item>
          <Menu.Item>
            <a href="/#"> 로그인 </a>
          </Menu.Item>
          <Menu.Item>
            <a href="/#"> SideNav </a>
          </Menu.Item>
          <Menu.Item>
            <Input.Search enterButton style={{ verticalAlign: "middle" }} />
          </Menu.Item>
          <Menu.Item>
            <a href="/#"> 브랜드 </a>
          </Menu.Item>
        </Menu>

        <Row gutter={8}>
          <Col xs={24} md={6}>
            <Card
              actions={[
                <div key="twit">
                  짹짹
                  <br />
                  {dummy.Post.length}
                </div>,
                <div key="following">
                  팔로잉
                  <br />
                  {dummy.Followings.length}
                </div>,
                <div key="follower">
                  팔로워
                  <br />
                  {dummy.Followers.length}
                </div>,
              ]}
            >
              <Card.Meta
                avatar={<Avatar>{dummy.nickname[0]}</Avatar>}
                title={dummy.nickname}
              />
            </Card>
            <Link href="/signup"></Link>
          </Col>
          <Col xs={24} md={12}>
            {children}
          </Col>
          <Col xs={24} md={6}>
            세번째
          </Col>
          <Row gutter={8}>
            <Col xs={24} md={12}>
              네번째
            </Col>
            <Col xs={24} md={12}>
              다섯번째
            </Col>
          </Row>
        </Row>
        <hr />

        <hr />

        <hr />
        {/* 24가 모든 단위에서의 한칸을 의미.  xs=24, md=24 에서 24는 grid한줄(max)을 의미. */}
        <Row gutter={7}>
          <Col
            style={{
              border: "1px solid black",
            }}
            xs={24}
            sm={2}
            md={8}
            lg={8}
            xl={8}
          >
            <MenuOutlined />
          </Col>
          <Col
            style={{
              border: "1px solid black",
            }}
            xs={8}
            sm={22}
            md={8}
            lg={8}
            xl={8}
          >
            Col2
          </Col>
        </Row>
      </div>
      <hr />
      HEADER
      <div
        style={{
          border: "1px solid blue",
          // justifyContent: "center",
          // height: "50px",
          display: "flex",
          textAlign: "center",
        }}
      >
        <div
          style={{
            border: "1px solid black",
          }}
        >
          <Button icon={<MenuOutlined />}></Button>
        </div>
        <div
          style={{
            border: "1px solid black",
          }}
        >
          <Button icon={<SearchOutlined />}></Button>
        </div>

        <div
          style={{
            width: "100%",
            // border: "1px solid black",
            margin: "auto 0",
            color: "black",
          }}
        >
          {/* <Button type="">LEEGUNSHOP</Button> */}
          <Link to="/" className={styles.logo}>
            LEEGUNSHOP
          </Link>
        </div>
        <div
          style={{
            border: "1px solid black",
          }}
        >
          <Button icon={<UserOutlined />}></Button>
        </div>
        <div
          style={{
            border: "1px solid black",
          }}
        >
          <Button icon={<ShoppingCartOutlinedIcon />}></Button>
        </div>
      </div>
      <hr />
      <hr />
      test4
      <div>
        <div></div>
        <div>
          <div></div>
          <div></div>
        </div>
      </div>
      <div className={styles.menuContainer}>
        <div className={styles.menubtn}>
          <Button icon={<MenuOutlined />}></Button>
        </div>
        <div className={styles.searchbtn}>
          <Button icon={<SearchOutlined />}></Button>
        </div>
        <div className={styles.logo}>
          <Link> LEEGUNSHOP </Link>
        </div>
        <div className={styles.userbtn}>
          <Button icon={<UserOutlined />}></Button>
        </div>
        <div className={styles.cartbtn}>
          <Button icon={<ShoppingCartOutlinedIcon />}></Button>
        </div>

        <div className={styles.bigmenu1}>
          <ul
            style={{
              listStyle: "none",
              display: "flex",
              // margin: "0",
              margin: "auto 0",
              // justifyContent: "center",
            }}
          >
            <li className={styles.menuli}>
              <Link> Brand </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Shop </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Community </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Event </Link>
            </li>
          </ul>
        </div>
        <div className={styles.bigmenu2}>
          <ul
            style={{
              listStyle: "none",
              display: "flex",
              margin: "auto 0",
            }}
          >
            <li className={styles.menuli}>
              <Link> Login </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Join </Link>
            </li>
          </ul>
        </div>
      </div>
      test5
      <Row
        style={{
          border: "1px solid black",
          height: "50px",
          // margin: "auto 0",
        }}
      >
        <Col
          lg={4}
          xl={4}
          md={4}
          style={{
            textAlign: "center",
            fontSize: "20px",
            margin: "auto 0",
          }}
        >
          <Link> LEEGUMSHOP </Link>
        </Col>
        <Col
          lg={14}
          xl={14}
          md={14}
          style={{
            fontSize: "17px",
            margin: "auto 0",
          }}
        >
          <Row gutter={32}>
            <Col> Brand </Col>
            <Col> Shop </Col>
            <Col>
              {/* <Dropdown overlay={menu}>
                <a onClick={(e) => e.preventDefault()}>
                  <Space>Community</Space>
                </a>
              </Dropdown> */}

              <Dropdown
                overlay={menuCommunity}
                placement="bottomLeft"
                // arrow={{ pointAtCenter: true }}
              >
                <Space
                  style={{
                    cursor: "pointer",
                  }}
                >
                  Community
                </Space>
              </Dropdown>
            </Col>
            <Col> Event </Col>
          </Row>
        </Col>
        <Col
          lg={6}
          xl={6}
          md={6}
          style={{
            fontSize: "17px",
            margin: "auto 0",
            textAlign: "right",
          }}
          offset={0}
        >
          <Row gutter={12}>
            <Col> LOGIN </Col>
            <Col> Join </Col>
            <Col>
              <UserOutlined />
            </Col>
            <Col>
              <ShoppingCartOutlined />
            </Col>
            <Col>
              <SearchOutlined />
            </Col>
          </Row>
        </Col>
      </Row>
      <hr />
      <Dropdown
        overlay={menu}
        placement="bottomLeft"
        // arrow={{ pointAtCenter: true }}
      >
        <Button>bottomLeft</Button>
      </Dropdown>
      <Dropdown
        overlay={menu}
        placement="bottomLeft"
        // arrow={{ pointAtCenter: true }}
      >
        <Button>bottomRight</Button>
      </Dropdown>
      <hr />
      <Row justify="end" align="middle">
        <Col
          // flex={"30px"}
          lg={4}
          xl={4}
          md={4}
          style={{
            border: "1px solid black",
            textAlign: "center",
            fontSize: "24px",
          }}
        >
          LEEGUNSHOP
        </Col>
        <Col lg={20} xl={20} md={20} style={{ marginLeft: "auto" }}>
          <Menu
            mode="horizontal"
            className={styles.menu}
            key="menu1"
            overflowedIndicator={<MenuOutlined />}
            selectable="false"
            triggerSubMenuAction="click"
            style={{
              marginLeft: "auto",
              fontSize: "18px",
            }}
          >
            <Menu.Item>
              <a href="/#"> Brand </a>
            </Menu.Item>
            <Menu.Item>
              <a href="/#"> Shop </a>
            </Menu.Item>
            <Menu.Item>
              <a href="/#"> Community </a>
            </Menu.Item>
            <Menu.Item
              style={
                {
                  // marginRight: "10rem",
                }
              }
            >
              <a href="/#"> Event </a>
            </Menu.Item>
            <Menu.Item>
              <a href="/#"> SideNav </a>
            </Menu.Item>

            <Menu.Item>
              <a href="/#"> 브랜드 </a>
            </Menu.Item>
          </Menu>
        </Col>
      </Row>
      <hr />
      sm: 576px, md: 768px, lg: 992px, xl: 1200px
      <br></br>
      Current break point:
      {Object.entries(screens)
        .filter((screen) => !!screen[1])
        .map((screen) => (
          <Tag color="blue" key={screen[0]}>
            {screen[0]}
          </Tag>
        ))}
    </>
  );
};

export default Test;
