import React from "react";
import {
  DaumPostcodeEmbed,
  useDaumPostcodePopup,
  loadPostcode,
} from "react-daum-postcode";
import styles from "./daumpost.module.css";

const Daumpost = (props) => {
  //   const { open, close } = props;
  const scriptUrl =
    "https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js";
  const open = useDaumPostcodePopup(scriptUrl);

  const postCodeStyle = {
    // display: "block",
    // position: "absolute",
    // top: "50%",
    // left: "50%",
    // width: "40%",
    // height: "500px",
    // transform: "translate(-50%, -50%)",
    // padding: "7px",
    // border: "1px solid black",
    // backgroundColor: "rgba(0,0,0,0.25)",
  };

  const handleComplete = (data) => {
    let fullAddress = data.address;
    let extraAddress = "";

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress +=
          extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }

    console.log(fullAddress); // e.g. '서울 성동구 왕십리로2길 20 (성수동1가)'
  };
  const handleClick = () => {
    open({ onComplete: handleComplete });
  };
  return (
    <div className={styles.container}>
      <button type="button"> X </button>
      <DaumPostcodeEmbed
        onComplete={handleComplete}
        // className={styles.postmodal}
        style={postCodeStyle}
        autoClose
      />

      {/* <button type="button" onClick={handleClick}>
        Open
      </button> */}
    </div>
  );
};

export default Daumpost;
