import React, { useState } from "react";
import styles from "./join.module.css";
import { useNavigate } from "react-router-dom";
import commStyles from "../../common/common.module.css";
import {
  DaumPostcode,
  DaumPostcodeEmbed,
  useDaumPostcodePopup,
} from "react-daum-postcode";
import Daumpost from "../daumpost/daumpost";
import axiosClient from "../../apis/index.js";

const Join = () => {
  //   const [id, setId] = useState("");
  //   const [password, setPassword] = useState("");
  //   const [chkPassword, setChkPassword] = useState("");
  //   const [name, setName] = useState("");
  //   const [phone, setPhone] = useState("");
  //   const [email, setemail] = useState("");
  //   const [postcode, setPostcode] = useState("");
  //   const [addr1, setAddr1] = useState("");
  //   const [addr2, setAddr2] = useState("");

  // 비밀번호 재확인
  const [isChkPass, setIsChkPass] = useState(true);
  // 비밀번호 유효성
  const [isChkPassValid, setIsChkPassValid] = useState(true);
  // 아이디 유효성
  const [isChkIdValid, setIsChkIdValid] = useState(true);
  // 이메일 유효성
  const [isChkEmailValid, setIsChkEmailValid] = useState(true);
  // 인증번호 여부
  const [isChkAuthValid, setIsChkAuthValid] = useState(true);
  // 인증결과 여부
  const [isAuthComplete, setIsAuthComplete] = useState(false);

  const [errTextId, setErrTextId] = useState("");
  const [errTesxPassWord, setErrTextPassWord] = useState("");
  const [errTextEmail, setErrTextEmail] = useState("");
  const [subTextAuthNumber, setSubTextAuthNumber] = useState("");
  const [subTextAuthComplete, setSubTextAuthComplete] = useState("");

  // const [authNumber, setAuthNumber] = useState("");
  const navigate = useNavigate();

  const [form, setForm] = useState({
    id: "",
    password: "",
    chkPassword: "",
    name: "",
    gender: "male",
    phone: "",
    authNumber: "",
    email: "",
    postcode: "",
    addr1: "",
    addr2: "",
  });

  const {
    id,
    password,
    chkPassword,
    name,
    gender,
    phone,
    authNumber,
    email,
    postcode,
    addr1,
    addr2,
  } = form;

  console.log(`postcode: ${postcode}`);
  console.log(`id: ${id}`);
  const onChange = (e) => {
    const { value, name } = e.target;
    console.log(value);
    console.log(name);
    console.log(id);
    setForm({
      ...form,
      [name]: value,
    });
  };

  // 아이디 중복 체크
  const onValidId = (e) => {
    console.log("onValidId");
    console.log(id);
    // const id = e.target.value;

    console.log(typeof id);
    if (id === "") {
      setErrTextId("아이디를 입력해주세요.");
      setIsChkIdValid(false);
      return;
    }
    // const regex = /^[a-z|A-Z|0-9|{4,16}]+$/;
    const regex = /^[a-z]{1}[a-z0-9]{3,15}$/;

    const regex1 = /^[ㄱ-ㅎ-가-힣-A-Z0-9]+$/; // 한글&영어(대문자)&숫자
    const regex2 = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/g; // 특수문자 정규식
    const regex3 = /^[A-Za-z0-9-ㄱ-ㅎ-가-힣]{0,3}$/; // 0~3자리수
    const regex4 = /^[0-1]{1}[a-zA-Z0-9]{3,15}$/; // 1자리 숫자
    const regex5 = /^[a-z]{1}[a-zA-Z0-9]{3,15}$/; // 1자리 숫자
    const regex6 = /\s/g; // 공백
    const regex7 = /^[a-z]{1}[a-z0-9]{16,}$/; // 1자리 숫자

    console.log(regex.test(id));
    console.log(regex7.test(id));

    const regrx8 = /^[a-z0-9]{4,16}$/;
    console.log("=====================");
    console.log(regrx8.test(id));

    if (!regex.test(id)) {
      console.log(regex3.test(id));
      if (regex3.test(id) || regex7.test(id)) {
        setErrTextId("아이디는 영문소문자 또는 숫자 4~16자로 입력해 주세요.");
      } else if (
        regex1.test(id) ||
        regex2.test(id) ||
        regex4.test(id) ||
        regex5.test(id) ||
        regex6.test(id)
      ) {
        setErrTextId(
          "대문자/공백/특수문자가 포함되었거나, 숫자로 시작 또는 숫자로만 이루어진 아이디는 사용할 수 없습니다."
        );
      } else {
      }
      setIsChkIdValid(false);
      return false;
    } else {
      setIsChkIdValid(true);
      setErrTextId("");
    }

    axiosClient
      .get("/user/chkValidId", {
        params: {
          id: id,
        },
      })
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          // id 중복아님
          if (res.data === "valid") {
          } else {
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // 비밀번호 유효성 체크
  const onValidPassword = () => {
    console.log("onValidPassword");
    const regPass =
      /^(?!((?:[A-Za-z]+)|(?:[~!@#$%^&*()_+=]+)|(?:[0-9]+))$)[A-Za-z\d~!@#$%^&*()_+=]{8,16}$/; // 영문,특수문자,숫자 2가지 이상
    const regLength = /^[A-Za-z0-9-ㄱ-ㅎ-가-힣\d~!@#$%^&*()_+=]{8,16}$/;

    console.log(regLength.test(password));
    console.log(regPass.test(password));
    setIsChkPassValid(true);
    setErrTextPassWord("");
    // 자리수 4자리 이하
    if (!regLength.test(password)) {
      setErrTextPassWord("8~16자 이내로 입력해 주십시오.");
      setIsChkPassValid(false);
      return false;
    }
    if (!regPass.test(password)) {
      setErrTextPassWord(
        "숫자,영문 대소문자,특수문자 중 두가지 이상으로 조합해 주십시오."
      );
      setIsChkPassValid(false);
    }
  };

  // 비밀번호 다시 확인
  const onChkPassword = () => {
    console.log("onblur");
    console.log(password);
    console.log(chkPassword);
    if (password !== chkPassword) {
      setIsChkPass(false);
    } else {
      setIsChkPass(true);
    }
  };

  // 이메일 체크
  const onValidEmail = () => {
    let regExp =
      /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

    console.log(regExp.test(email));
    if (regExp.test(email)) {
      setIsChkEmailValid(true);
      setErrTextEmail("");
    } else {
      setIsChkEmailValid(false);
      setErrTextEmail("올바른 이메일 형식이 아닙니다. ");
    }
  };

  const scriptUrl =
    "https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js";
  const open = useDaumPostcodePopup(scriptUrl);

  // 회원가입 신청 Submit
  const handleSubmit = (event) => {
    event.preventDefault();

    if (id === "" || !isChkIdValid) {
      alert("아이디를 다시 입력해주세요.");
      return;
    }
    if (password === "" || !isChkPassValid) {
      alert("비밀번호를 다시 입력해주세요.");
      return;
    }
    if (chkPassword === "" || !isChkPass) {
      alert("비밀번호를 다시 입력해주세요.");
      return;
    }
    if (email === "" || !isChkEmailValid) {
      alert("이메일을 다시 입력해주세요.");
      return;
    }
    if (name === "") {
      alert("이름을 입력해주세요.");
      return;
    }
    if (gender === "") {
      alert("성별을 입력해주세요.");
      return;
    }
    if (phone === "") {
      alert("전화번호를 입력해주세요.");
      return;
    }

    if (postcode === "" && addr1 === "") {
      alert("주소를 입력해주세요.");
      return;
    }
    if (addr2 === "") {
      alert("상세주소를 입력해주세요.");
      return;
    }

    if (!isAuthComplete) {
      alert("본인인증이 완료되지 않았습니다. ");
      return;
    }

    console.log(form);

    axiosClient
      .post(`/user/join`, {
        form,
      })
      .then((res) => {
        console.log(res);
        if (res.data === "success") {
          alert("회원가입이 완료되었습니다. ");
          navigate("/login");
        }
      });
  };

  // 인증번호 전송 함수
  const handleSendSMS = () => {
    axiosClient
      .post(`/user/message/mAuth`, {
        phone,
      })
      .then((res) => {
        console.log(res);
        // 인증번호가 정상적으로 response됨
        if (res.data === "success" && res.status === 200) {
          setIsChkAuthValid(false);
          setSubTextAuthNumber(
            "[3분 이내 인증] 고객님의 휴대폰으로 인증번호를 전송했습니다. "
          );
        }
      })
      .catch((err) => {
        console.log(err.response.data);
        alert(err.response.data);
      });
  };

  // 인증번호 확인 함수
  const handleConfirmAuth = (e) => {
    console.log(e.target);
    console.log(authNumber);
    if (isAuthComplete) {
      alert("인증이 이미 완료되었습니다. ");
      return;
    }
    if (authNumber !== "") {
      axiosClient
        .post(`/user/message/confirmVerifyCode`, {
          phone,
          authNumber,
        })
        .then((res) => {
          console.log(res);
          // 인증성공
          if (res.status === 200) {
            alert(res.data);
            setIsAuthComplete(true);
            setSubTextAuthComplete(res.data);
          } else {
            alert(res.data);
            setIsAuthComplete(false);
            setSubTextAuthComplete(res.data);
          }
        })
        .catch((err) => {
          alert(err.response.data);
          setSubTextAuthComplete(err.response.data);
        });
    } else {
      alert("인증번호를 입력해주세요. ");
    }
  };

  // daum post popup
  const handlePostClick = () => {
    open({ onComplete: handleComplete });
  };

  const handleComplete = (data) => {
    let fullAddress = data.address;
    let extraAddress = "";
    let postcode2 = data.zonecode;

    const name = "postcode";
    const name2 = "addr1";

    console.log(data);
    // setPostcode(() => postcode);

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddress += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddress +=
          extraAddress !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddress += extraAddress !== "" ? ` (${extraAddress})` : "";
    }
    // setAddr1(() => fullAddress);

    setForm({
      ...form,
      [name]: postcode2,
      [name2]: fullAddress,
      //   [addr1]: fullAddress,
    });
    console.log(fullAddress); // e.g. '서울 성동구 왕십리로2길 20 (성수동1가)'
    console.log(form);
  };

  return (
    <div className={styles.container}>
      <h2
        style={{
          textAlign: "center",
        }}
      >
        회원 가입
      </h2>

      <div className={styles.join_form}>
        <div className={commStyles.line_green}></div>
        <form onSubmit={handleSubmit} className={styles.form}>
          <table className={styles.table}>
            <thead></thead>
            <tbody>
              <tr className={styles.tr}>
                <th className={`${styles.required_red} ${styles.th}`}>
                  아이디
                </th>
                <td className={styles.td}>
                  <div>
                    <input
                      type="text"
                      name="id"
                      className={styles.input_text}
                      placeholder="아이디를 입력해주세요."
                      value={id}
                      onChange={onChange}
                      onBlur={onValidId}
                    />
                  </div>
                  {!isChkIdValid && (
                    <div
                      style={{
                        width: "90%",
                      }}
                    >
                      <span className={styles.txt_error}>{errTextId}</span>
                    </div>
                  )}
                  {/* <span className={styles.txt_error}> test </span> */}
                  <div className={styles.txt_info}>
                    - 영문소문자 또는 숫자 4~16자로 입력해 주세요.
                  </div>
                </td>
              </tr>
              {/* <tr className={styles.line}> </tr> */}
              <tr>
                <th className={`${styles.th} ${styles.required_red}`}>
                  비밀번호
                </th>
                <td className={styles.td}>
                  <div>
                    <input
                      type="password"
                      name="password"
                      className={styles.input_text}
                      placeholder="비밀번호를 입력해 주세요."
                      value={password}
                      onChange={onChange}
                      onBlur={onValidPassword}
                    />
                  </div>
                  {!isChkPassValid && (
                    <span className={styles.txt_error}>{errTesxPassWord}</span>
                  )}
                  <div className={styles.txt_info}>
                    - 대소문자/숫자/특수문자 조합, 8자~16자
                  </div>
                </td>
              </tr>
              <tr>
                <th className={`${styles.required_red} ${styles.th}`}>
                  비밀번호 확인
                </th>
                <td className={styles.td}>
                  <div>
                    <input
                      type="password"
                      name="chkPassword"
                      className={styles.input_text}
                      placeholder="비밀번호를 다시 한번 입력해 주세요."
                      value={chkPassword}
                      onChange={onChange}
                      onBlur={onChkPassword}
                    />
                  </div>
                  {!isChkPass && (
                    <span className={styles.txt_error}>
                      비밀번호가 일치하지 않습니다.
                    </span>
                  )}
                </td>
              </tr>
              <tr>
                <th className={`${styles.required_red} ${styles.th}`}>
                  이메일
                </th>
                <td className={styles.td}>
                  <input
                    type="text"
                    name="email"
                    className={styles.input_text}
                    placeholder="이메일을 입력해 주세요."
                    value={email}
                    onChange={onChange}
                    onBlur={onValidEmail}
                  />
                  {!isChkEmailValid && (
                    <div
                      style={{
                        width: "90%",
                      }}
                    >
                      <span className={styles.txt_error}>{errTextEmail}</span>
                    </div>
                  )}
                </td>
              </tr>
              <tr>
                <th className={styles.th}>이름</th>
                <td className={styles.td}>
                  <input
                    type="text"
                    name="name"
                    className={styles.input_text}
                    placeholder="이름을 입력해 주세요."
                    value={name}
                    onChange={onChange}
                  />
                </td>
              </tr>
              <tr>
                <th className={styles.th}>성별</th>
                <td className={styles.td}>
                  <input
                    type="radio"
                    name="gender"
                    id="male"
                    className={styles.input_radio}
                    onChange={onChange}
                    value="male"
                    defaultChecked
                  />
                  <label htmlFor="gender" className={styles.label_gender}>
                    남
                  </label>
                  <input
                    type="radio"
                    name="gender"
                    id="female"
                    className={styles.input_radio}
                    onChange={onChange}
                    value="female"
                  />
                  <label htmlFor="gender" className={styles.label_gender}>
                    여
                  </label>
                </td>
              </tr>
              <tr>
                <th className={styles.th}>휴대전화</th>
                <td className={styles.td}>
                  <ul className={styles.ul}>
                    <li className={styles.li_col2}>
                      <input
                        type="number"
                        name="phone"
                        className={`${commStyles.input_col2_7} ${styles.input_postcode}`}
                        value={phone}
                        onChange={onChange}
                        placeholder="휴대전화를 입력해 주세요."
                        // disabled="true"
                      />

                      <button
                        type="button"
                        className={`${commStyles.btn_col2_1} ${styles.btn_addr}`}
                        onClick={handleSendSMS}
                      >
                        인증번호받기
                      </button>
                    </li>
                    {!isChkAuthValid && (
                      <li className={styles.li_col1}>
                        <span className={styles.txt_sub}>
                          {subTextAuthNumber}
                        </span>
                      </li>
                    )}
                    {!isChkAuthValid && (
                      <li className={styles.li_col2}>
                        <input
                          type="text"
                          name="authNumber"
                          className={`${commStyles.input_col2_7} ${styles.input_postcode}`}
                          placeholder="인증번호 입력"
                          value={authNumber}
                          onChange={onChange}
                          // onClick={handleConfirmAuth}
                        />
                        <button
                          type="button"
                          className={`${commStyles.btn_col2_1} ${styles.btn_addr}`}
                          onClick={handleConfirmAuth}
                        >
                          인증번호 확인
                        </button>
                      </li>
                    )}

                    {isAuthComplete && (
                      <li className={styles.li_col1}>
                        <span className={styles.txt_sub}>
                          {subTextAuthComplete}
                        </span>
                      </li>
                    )}
                  </ul>
                </td>
              </tr>
              <tr>
                <th className={styles.th}> 주소 </th>
                <td className={styles.td}>
                  <ul className={styles.ul}>
                    <li className={styles.li_col2}>
                      <input
                        type="text"
                        className={`${commStyles.input_col2_7} ${styles.input_postcode}`}
                        defaultValue={postcode}
                        placeholder="우편번호"
                        disabled={true}
                      />

                      <button
                        type="button"
                        className={`${commStyles.btn_col2_1} ${styles.btn_addr}`}
                        onClick={handlePostClick}
                      >
                        주소검색
                      </button>

                      <div></div>
                    </li>
                    <li className={styles.li_col1}>
                      <input
                        type="text"
                        className={`${styles.input_text} `}
                        defaultValue={addr1}
                        placeholder="기본주소"
                        disabled={true}
                      />
                    </li>
                    <li className={styles.li_col1}>
                      <input
                        type="text"
                        name="addr2"
                        className={`${styles.input_text} `}
                        placeholder="상세주소"
                        value={addr2}
                        onChange={onChange}
                      />
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <td colSpan="2" className={commStyles.text_center}>
                  <input
                    type="submit"
                    value="회원가입"
                    className={styles.btnJoin}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  );
};

export default Join;
