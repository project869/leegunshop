import {
  BarsOutlined,
  CalculatorOutlined,
  EuroOutlined,
  HeartOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import React, { useState } from "react";

const Drawer = () => {
  const [visible, setVisible] = useState(false);
  const onClose = () => {
    setVisible(false);
  };
  return (
    <Drawer
      title="LEEGUNSHOP"
      placement="left"
      onClose={onClose}
      visible={visible}
    >
      <div style={{ display: "flex", flexDirection: "column" }}>
        <p>adasd </p>
        <Button type="text" href="/finances" icon={<EuroOutlined />}>
          Finances
        </Button>

        <Button type="text" href="/sante" icon={<HeartOutlined />}>
          Santé
        </Button>
        <Button type="text" href="/mathematiques" icon={<CalculatorOutlined />}>
          Mathématiques
        </Button>
        <Button type="text" href="/autres" icon={<BarsOutlined />}>
          Autres
        </Button>
      </div>
    </Drawer>
  );
};

export default Drawer;
