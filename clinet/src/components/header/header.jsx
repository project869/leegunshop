import { Layout } from "antd";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";

import React from "react";
import styles from "./header.module.css";
import Navbar from "../navbar/navbar";

const Header = () => {
  return (
    <>
      <header className={styles.header}>
        <Navbar />
      </header>
      {/*     
      <Layout>
        <header className={styles.header}>
          <div className={styles.leftContainer}>
            <h1 className={styles.h1}> LeeGunShop </h1>

            <ul className={styles.ul}>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  Brand
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  Shop
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  Community
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  Event
                </a>
              </li>
            </ul>
          </div>
          <div className={styles.rightContainer}>
            <ul className={styles.ul}>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  LOGIN
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  JOIN
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  MY SHOPPING
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  <ShoppingCartOutlinedIcon />
                </a>
              </li>
              <li className={styles.li}>
                <a href="#!" className={styles.a}>
                  <SearchOutlinedIcon />
                </a>
              </li>
            </ul>
          </div>
        </header>
      </Layout> */}
    </>
  );
};

export default Header;
