import React, { useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import SnsLoginForm from "../sns_login_form/snsLoginForm";
import styles from "./login.module.css";
import axiosClient from "../../apis/index.js";

const Login = () => {
  const [account, setAccount] = useState({
    id: "",
    password: "",
  });

  const { id, password } = account;

  const navigate = useNavigate();
  const handleSubmit = (event) => {
    event.preventDefault();

    // id,password 유효성 검사
    if (id === "") alert("아이디를 입력하세요.");
    else if (password === "") alert("비밀번호를 입력하세요.");

    console.log(account);
    axiosClient
      .get("/user/getLogin", {
        params: account,
      })
      .then((res) => {
        console.log("then");
        console.log(res);
        if (res.status === 200) {
          alert(res.data.msg);
        }
      })
      .catch((err) => {
        console.log(err);
        alert(err.response.data);
      });
  };

  const onChange = (e) => {
    const { value, name } = e.target;
    console.log(value);
    console.log(name);

    setAccount({
      ...account,
      [name]: value,
    });

    console.log(account);
  };

  //
  const handleJoin = () => navigate("/join");

  return (
    <div className={styles.container}>
      {/* <Outlet /> */}

      <h3> LOGIN </h3>
      <div className={styles.login_form}>
        <form onSubmit={handleSubmit} className={styles.form}>
          <div>
            <input
              type="text"
              name="id"
              className={styles.input}
              value={id}
              placeholder="아이디"
              onChange={onChange}
            />
          </div>
          <div>
            <input
              type="password"
              name="password"
              className={styles.input}
              value={password}
              placeholder="비밀번호"
              onChange={onChange}
            />
          </div>
          <div
            style={{
              textAlign: "right",
              marginBottom: "5px",
            }}
          >
            <span>
              <Link
                to="/"
                style={{
                  color: "black",
                }}
              >
                아이디 찾기
              </Link>
            </span>
            <span> &nbsp; | &nbsp;</span>
            <span>
              <Link
                to="/"
                style={{
                  color: "black",
                }}
              >
                비밀번호 찾기
              </Link>
            </span>
          </div>
          <input type="submit" value="로그인" className={styles.btnSubmit} />

          <Link to="/join">
            <input
              type="button"
              value="회원가입"
              className={styles.btnJoin}
              onClick={handleJoin}
            />
          </Link>
        </form>
        <hr />
        <SnsLoginForm />
      </div>
    </div>
  );
};

export default Login;
