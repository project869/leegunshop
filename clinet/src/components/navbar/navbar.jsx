import {
  BarsOutlined,
  CalculatorOutlined,
  EuroOutlined,
  HeartOutlined,
  MenuOutlined,
  SearchOutlined,
  UserOutlined,
} from "@ant-design/icons";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";
import { Button, Drawer, Dropdown, Image, Menu, Space, Carousel } from "antd";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styles from "./navbar.module.css";
import logoPic from "../../logo192.png";
import navDrawer from "../drawer/drawer";

const Navbar = () => {
  const [current, setCurrent] = useState("mail");
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    console.log("useEffect");
  }, []);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const handleClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  const contentStyle = {
    height: "280px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
  };

  const menuCommunity = (
    <Menu
      style={{
        backgroundColor: "black",
      }}
      items={[
        {
          key: "1",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.antgroup.com"
            >
              공지사항
            </a>
          ),
        },
        {
          key: "2",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.aliyun.com"
            >
              상품 Q&A
            </a>
          ),
        },
        {
          key: "3",
          label: (
            <a
              className={styles.dropMenuItem}
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.luohanacademy.com"
            >
              자유게시판
            </a>
          ),
        },
      ]}
    />
  );
  return (
    <>
      <div className={styles.menuContainer}>
        <div className={styles.menubtn} onClick={showDrawer}>
          <Button icon={<MenuOutlined />}></Button>
        </div>
        <div className={styles.searchbtn}>
          <Button icon={<SearchOutlined />}></Button>
        </div>
        <div className={styles.logo}>
          <Link to="/">
            <h2>LEEGUNSHOP</h2>
          </Link>
        </div>
        <div className={styles.userbtn}>
          <Link to="/login">
            <Button icon={<UserOutlined />}></Button>
          </Link>
        </div>
        <div className={styles.cartbtn}>
          <Button icon={<ShoppingCartOutlinedIcon />}></Button>
        </div>

        <div className={styles.bigmenu1}>
          <ul
            style={{
              listStyle: "none",
              display: "flex",
              // margin: "0",
              margin: "auto 0",
              // justifyContent: "center",
            }}
          >
            <li className={styles.menuli}>
              <Link> Brand </Link>
            </li>
            <li className={styles.menuli}>
              <Dropdown
                overlay={menuCommunity}
                placement="bottomLeft"
                // arrow={{ pointAtCenter: true }}
              >
                <Space
                  style={{
                    cursor: "pointer",
                  }}
                >
                  Community
                </Space>
              </Dropdown>
            </li>
            <li className={styles.menuli}>
              <Link> Community </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Event </Link>
            </li>
          </ul>
        </div>
        <div className={styles.bigmenu2}>
          <ul
            style={{
              listStyle: "none",
              display: "flex",
              margin: "auto 0",
            }}
          >
            <li className={styles.menuli}>
              <Link to="/login"> Login </Link>
            </li>
            <li className={styles.menuli}>
              <Link> Join </Link>
            </li>
          </ul>
        </div>

        {/* <navDrawer /> */}

        <Drawer
          title="LEEGUNSHOP"
          placement="left"
          onClose={onClose}
          visible={visible}
        >
          <div style={{ display: "flex", flexDirection: "column" }}>
            <p>adasd </p>
            <Button type="text" href="/finances" icon={<EuroOutlined />}>
              Finances
            </Button>

            <Button type="text" href="/sante" icon={<HeartOutlined />}>
              Santé
            </Button>
            <Button
              type="text"
              href="/mathematiques"
              icon={<CalculatorOutlined />}
            >
              Mathématiques
            </Button>
            <Button type="text" href="/autres" icon={<BarsOutlined />}>
              Autres
            </Button>
          </div>
        </Drawer>
      </div>

      {/* <div>
        <Carousel autoplay>
          <div>
            <h3 style={contentStyle}>1</h3>
          </div>
          <div>
            <h3 style={contentStyle}>2</h3>
          </div>
          <div>
            <h3 style={contentStyle}>3</h3>
          </div>
          <div>
            <h3 style={contentStyle}>4</h3>
          </div>
        </Carousel>
      </div> */}
      {/* <div className={styles.container}>
        <div>
          <Button
            className={styles.menubtn}
            type="primary"
            shape="circle"
            icon={<MenuOutlined />}
            onClick={showDrawer}
          ></Button>
        </div>

        <div
          style={{
            width: "100%",
            textAlign: "center",
            border: "1px solid pink",
          }}
        >
          <Link to="/" className={styles.logo}>
            LEEGUNSHOP 11
          </Link>
        </div>

        <Menu
          className={styles.bigmenu}
          onClick={handleClick}
          selectedKeys={[current]}
          mode="horizontal"
          overflowedIndicator={<MenuOutlined />}
        >
          <Menu.Item key="finance" icon={<EuroOutlined />}>
            <Link to="/login"> Finances </Link>
          </Menu.Item>
          <Menu.Item key="santé" icon={<HeartOutlined />}>
            <Link to="/login"> Santé </Link>
          </Menu.Item>
          <Menu.Item key="apmathsp" icon={<CalculatorOutlined />}>
            <Link to="/login"> Mathématiques </Link>
          </Menu.Item>
          <Menu.Item key="autres" icon={<BarsOutlined />}>
            Autres
          </Menu.Item>
        </Menu>

        <Drawer
          title={<Image src={logoPic} alt="logo" />}
          placement="left"
          onClose={onClose}
          visible={visible}
        >
          <div style={{ display: "flex", flexDirection: "column" }}>
            <Button type="text" href="/finances" icon={<EuroOutlined />}>
              Finances
            </Button>

            <Button type="text" href="/sante" icon={<HeartOutlined />}>
              Santé
            </Button>
            <Button
              type="text"
              href="/mathematiques"
              icon={<CalculatorOutlined />}
            >
              Mathématiques
            </Button>
            <Button type="text" href="/autres" icon={<BarsOutlined />}>
              Autres
            </Button>
          </div>
        </Drawer>
      </div> */}
    </>
  );
};

export default Navbar;
