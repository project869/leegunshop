import React from "react";
import KakaoLoginLogo from "../../images/kakao_login_medium_wide.png";
import KakaoLogo from "../../images/kakao-logo.svg";
import NaverLogo from "../../images/naver-logo.png";
import styles from "./snsLoginForm.module.css";

const SnsLoginForm = () => {
  const handleKakaoLogin = () => {
    alert();
  };
  const handleNaverLogin = () => {
    alert();
  };

  return (
    <div className={styles.container}>
      <h3
        style={{
          marginBottom: "15px",
        }}
      >
        SNS 로그인
      </h3>
      <div className={styles.btnKakaoLogin} onClick={handleKakaoLogin}>
        <img src={KakaoLogo} alt="" className={styles.logo} /> 카카오 로그인
      </div>
      <div className={styles.btnNaverLogin} onClick={handleNaverLogin}>
        <img src={NaverLogo} alt="" className={styles.logo} /> 네이버 로그인
      </div>
    </div>
  );
};

export default SnsLoginForm;
