const db = require("../config/database.js");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const cache = require("memory-cache");
// solapi api 설정
const { SolapiMessageService } = require("solapi");

// 난수 생성 함수
const randomNum = (n) => {
  let str = "";
  for (let i = 0; i < n; i++) {
    str += Math.floor(Math.random() * 10);
  }
  return str;
};

module.exports = {
  /**
   * id유효성 체크
   *
   * @param id
   * @returns {Promise<null>}
   */

  getChkValidId: async (id) => {
    console.log(id);
    let conn = null;
    let result = null;

    const query = `SELECT * FROM users WHERE id = '${id}'`;

    conn = await db.getConnection();

    try {
      let [rows] = await conn.query(query);
      if (rows && rows.length === 0) result = "invalid";
      else result = "valid";

      console.log(rows);
      return result;
    } catch (error) {
      console.error(error);
    }
  },
  // 본인인증 문자 보내는 함수
  sendAuthNum: async (phone) => {
    const sendNumber = process.env.SEND_NUMBER;
    const SOLAPI_API_KEY = process.env.SOLAPI_API_KEY;
    const SOLAPI_API_SECRET = process.env.SOLAPI_API_SECRET;

    const messageService = new SolapiMessageService(
      SOLAPI_API_KEY,
      SOLAPI_API_SECRET
    );
    let result = {};

    let authNum = randomNum(6);
    console.log(`인증번호생성: ${authNum}`);

    // 3분 = 180000
    cache.put(phone, authNum, 180000, function (key, value) {
      console.log("timeoutCallback()");
      console.log(key + " did " + value);
    });

    try {
      // await messageService.send({
      //   to: phone,
      //   from: sendNumber,
      //   text: `[LEEGUNSHOP] 인증번호 ${authNum} 를 입력해 주세요.`,
      // });
      result.msg = "success";
      result.code = 200;
    } catch (err) {
      console.error(err);
      cache.del(phone);
      result.msg = "Serve Error";
      result.code = 500;
    }
    return result;
  },
  // 본인인증 번호 확인 함수
  confirmVerifyCode: async (phone, authNumber) => {
    console.log(`phone: ${phone} verifyCode: ${authNumber}`);
    let result = {};

    const CacheData = cache.get(phone);
    console.log(CacheData);

    // 인증번호 만료
    if (!CacheData) {
      result.msg = "인증번호가 만료되었습니다. 다시 요청해주세요.";
      result.code = 400;
    } else if (CacheData !== authNumber) {
      result.msg = "인증번호가 올바르지 않습니다. 다시 입력해주세요.";
      result.code = 400;
    } else {
      result.msg = "인증이 완료되었습니다. ";
      result.code = 200;
      cache.del(phone);
    }

    return result;
  },
  // 회원가입
  joinProcess: async (form) => {
    console.log("JoinProcess");
    const { id, password, name, gender, phone, email, postcode, addr1, addr2 } =
      form;

    let conn = null;
    let result = {};
    let rows = null;

    // 데이터 유효성 검사는 추후 하기로 ..

    const query = `INSERT INTO users(id, password, email, name, gender, phone, postcode, addr1, addr2, create_date)
      VALUES ('${id}', SHA2('${password}', 256), '${email}', '${name}', '${gender}', '${phone}', '${postcode}', '${addr1}', '${addr2}', default);`;
    console.log(query);
    // const query = `INSERT INTO test VALUES ('3', 'asdf3', default)`;

    try {
      conn = await db.getConnection();
      rows = await conn.query(query);
      result.msg = "success";
      result.code = 200;
    } catch (error) {
      result.msg = error;
      result.code = 500;
    }

    console.log(rows);

    return result;
  },

  // 로그인
  getLogin: async (account) => {
    let conn = null;
    let result = {};
    let rows = null;

    let id = account.id;
    let password = account.password;

    console.log(id);
    const query = `SELECT * FROM USERS WHERE ID = '${id}' AND PASSWORD = SHA2('${password}', 256) `;

    try {
      conn = await db.getConnection();
      let [rows] = await conn.query(query);
      if (rows && rows.length === 0) {
        result.msg = "아이디나 비밀번호가 틀렸습니다.";
        result.code = 400;
      } else {
        // 로그인 성공
        // jwt 토큰 생성
        let token = jwt.sign(
          {
            id: id,
          },
          process.env.SECRET_KEY,
          {
            expiresIn: "1m",
          }
        );

        result.msg = "로그인이 성공했습니다. ";
        result.token = token;
        result.code = 200;
      }
      console.log(rows);
    } catch (err) {
      console.log(err);
    }
    return result;
  },
};
