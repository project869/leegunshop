const userService = require("../services/user.service.js");
require("express-async-errors");

exports.getIndex = async (req, res, next) => {
  res.render("index", { title: "Express" });
};
// id 유효성 검사
exports.getChkValidId = async (req, res, next) => {
  const id = req.query.id;
  let result = null;

  try {
    result = await userService.getChkValidId(id);
    res.send(result);
  } catch (err) {
    console.error(`error1: ${err}`);
  }
};

// 본인인증 인증번호
exports.sendAuthNum = async (req, res, next) => {
  let { phone } = req.body;
  let result = null;
  // try {
  //   result = await userService.sendAuthNum(phone);
  //   res.send(result);
  // } catch (err) {
  //   console.log(err);
  // }

  result = await userService.sendAuthNum(phone);

  res.status(result.code).json(result.msg);
  // res.send(result.msg);
};

// 본인인증 인증번호 확인
exports.confirmVerifyCode = async (req, res, next) => {
  const { phone, authNumber } = req.body;
  console.log(`${phone} | ${authNumber}`);
  let result = null;

  result = await userService.confirmVerifyCode(phone, authNumber);
  res.status(result.code).json(result.msg);
};
exports.joinProcess = async (req, res, next) => {
  console.log(`joinProcess`);
  console.log(req.body.form);
  const form = req.body.form;
  let result = null;

  result = await userService.joinProcess(form);
  res.status(result.code).json(result.msg);
};
exports.getLogin = async (req, res, next) => {
  console.log("getLogin");
  console.log(req.query);
  let account = req.query;
  let result = null;
  result = await userService.getLogin(account);
  res.status(result.code).json(result);
};
