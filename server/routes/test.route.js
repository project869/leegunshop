const express = require("express");
const router = express.Router();
const testController = require("../controller/test.controller.js");

// const connection = mysql.createConnection(dbconfig);

/* GET users listing. */
router.get("/", testController.getIndex);
router.get("/jwt", testController.getJwt);

module.exports = router;
