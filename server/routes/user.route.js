const express = require("express");
const router = express.Router();
const userController = require("../controller/user.controller.js");

/* GET users listing. */
router.get("/", userController.getIndex);
// ID유효성 체크
router.get("/chkValidId", userController.getChkValidId);
// 본인인증 인증번호 보내기
router.post("/message/mAuth", userController.sendAuthNum);
// 본인인증 확인
router.post("/message/confirmVerifyCode", userController.confirmVerifyCode);
// 회원가입
router.post("/join", userController.joinProcess);
// 로그인
router.get("/getLogin", userController.getLogin);

module.exports = router;
