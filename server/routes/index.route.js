const express = require("express");
const router = express.Router();
const indexController = require("../controller/index.controller.js");

/* GET home page. */
router.get("/", indexController.getIndex);

module.exports = router;
